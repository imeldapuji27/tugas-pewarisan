/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kurikulum;
import javax.swing.JOptionPane;


/**
 *
 * @author WAHYU
 */
public class KURIKULUM  {
    class K13{

//deklarasi
    private String model, program, standart;

    //setter
    public void setModel(String model)
    {        
        this.model=model;
    }
    public void setProgram(String program)
    {
        this.program=program;                
    }
    public void setStandart(String standart)
    {
        this.standart=standart;                
    }
    
    //getter
    public String getmodel()
    {
        return model;
    }
    public String getprogram()
    {
        return program;
    }
    public String getStandart()
    {
        return standart;
    }
    
    
    class KTSP{
        //deklarasi
    private String minat, mapel, konten;

    //setter

            public void setMinat(String minat) {
                this.minat = minat;
            }

            public void setMapel(String mapel) {
                this.mapel = mapel;
            }

            public void setKonten(String konten) {
                this.konten = konten;
            }
            
    //getter

            public String getMinat() {
                return minat;
            }

            public String getMapel() {
                return mapel;
            }

            public String getKonten() {
                return konten;
            }
            
    }
    }
    
    
    
        

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        int input = JOptionPane.showConfirmDialog(null, 
                " PEMROGRAMAN KURIKULUM " , " WELCOME ", JOptionPane.DEFAULT_OPTION);
        // 0=ok
                
        String ktsp = "KTSP", k13 = "K13";
        int pilihan = JOptionPane.showConfirmDialog(null, 
                "TERSEDIA KURIKULUM " + ktsp + " DAN " + k13 , "KURIKULUM", JOptionPane.DEFAULT_OPTION);
        // 0=ok
        
        //K13
        String tugas1 = " mewajibkan program pramuka,";
        String tugas2 = " menilai siswa secara otentik,";
        String tugas3 = " merubah durasi pembelajaran,";
        String tugas4 = " dan mengharuskan siswa lebih aktif dalam pembelajaran.";
        
        //KTSP
        String tugas5 = " menambah materi pembelajaran,";
        String tugas6 = " menekankan pada aspek pengetahuan,";
        String tugas7 = " memantapkan penilaian siswa,";
        String tugas8 = " dan memisahkan setiap konten pendidikan.";
        
        String kurikulum = JOptionPane.showInputDialog("Masukkan kurikulum yang tersedia!");
        JOptionPane.showMessageDialog(null, "Anda telah memilih "  + kurikulum);
        
        
       
        if (kurikulum != "k13"){
            int kurk13 = JOptionPane.showConfirmDialog(null, 
                " Program pada kurikulum ini yaitu: " + tugas1 + tugas2 + tugas3 + tugas4 , " K13 ", JOptionPane.DEFAULT_OPTION);
        }else {
            int kurktsp = JOptionPane.showConfirmDialog(null, 
                " Program pada kurikulum ini yaitu: " + tugas5 + tugas6 + tugas7 + tugas8 , " KTSP ", JOptionPane.DEFAULT_OPTION);
        }
        
        int keluar =  JOptionPane.showConfirmDialog(null, 
                "Program terhenti " , "EXIT", JOptionPane.DEFAULT_OPTION);
        
        
        }
    }